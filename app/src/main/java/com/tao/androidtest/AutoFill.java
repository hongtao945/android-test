package com.tao.androidtest;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.Nullable;

public class AutoFill extends Activity {

    private String[] items = {"北京","韶关","揭阳","汕头","潮州","乌鲁木齐","黑龙江","黑云压城城欲摧"};
    private AutoCompleteTextView display;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.autofill);
        this.init();
    }

    private void init() {
        this.display = findViewById(R.id.inputAutoFill);
        ArrayAdapter adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,items);//揭
        this.display.setAdapter(adapter);
    }
}
