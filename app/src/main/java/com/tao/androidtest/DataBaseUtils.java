package com.tao.androidtest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DataBaseUtils extends SQLiteOpenHelper {
    public DataBaseUtils(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table worker ("+
                "[id] integer not null primary key AUTOINCREMENT," +
                "[workId] nvarchar(12) not null default '',"+
                "[name] nvarchar(32) not null default '',"+
                "[age] nvarchar(12) not null default ''"+
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
