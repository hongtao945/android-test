package com.tao.androidtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button toCalc,toWeather,toDatabase,toAutoFill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }

    /**
     * 初始化
     */
    private void init() {
        // 选中按钮
        this.toCalc = (Button) findViewById(R.id.calc);
        this.toWeather = (Button) findViewById(R.id.weatherReport);
        this.toDatabase = (Button) findViewById(R.id.database);
        this.toAutoFill = (Button) findViewById(R.id.autoFill);
        // 为其添加响应事件
        this.toCalc.setOnClickListener(v -> {
            Intent calc = new Intent(MainActivity.this, CalculatorActivity.class);
            startActivity(calc);
        });

        this.toWeather.setOnClickListener(v -> {
            Intent weather = new Intent(MainActivity.this, Weather.class);
            startActivity(weather);
        });

        this.toAutoFill.setOnClickListener(v -> {
            Intent autoFill = new Intent(MainActivity.this, AutoFill.class);
            startActivity(autoFill);
        });

        this.toDatabase.setOnClickListener(v -> {
            Intent DataBaseTest = new Intent(MainActivity.this, DataBaseTest.class);
            startActivity(DataBaseTest);
        });
    }
}