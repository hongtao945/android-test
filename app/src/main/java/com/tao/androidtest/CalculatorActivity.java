package com.tao.androidtest;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.tao.androidtest.R;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_zero;
    private Button btn_one;
    private Button btn_two;
    private Button btn_three;
    private Button btn_four;
    private Button btn_five;
    private Button btn_six;
    private Button btn_seven;
    private Button btn_eight;
    private Button btn_night;
    private Button btn_add;
    private Button btn_sub;
    private Button btn_mul;
    private Button btn_div;
    private Button btn_ac;
    private Button btn_point;
    private Button btn_eq;
    private Button btn_del;
    private TextView textView_result;

    String val_1 = "";
    String val_2 = "";
    String string = "";
    String symbol = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator);
        this.init();
    }

    private void init() {

        //获取具体按钮
        btn_zero =  findViewById(R.id.num0);
        btn_one =  findViewById(R.id.num1);
        btn_two =  findViewById(R.id.num2);
        btn_three =  findViewById(R.id.num3);
        btn_four =  findViewById(R.id.num4);
        btn_five =  findViewById(R.id.num5);
        btn_six =  findViewById(R.id.num6);
        btn_seven =  findViewById(R.id.num7);
        btn_eight =  findViewById(R.id.num8);
        btn_night =  findViewById(R.id.num9);
        btn_add =  findViewById(R.id.add);
        btn_sub =  findViewById(R.id.subtraction);
        btn_mul =  findViewById(R.id.multiply);
        btn_div =  findViewById(R.id.division);
        btn_ac =  findViewById(R.id.clear);
        btn_point =  findViewById(R.id.dot);
        btn_eq =  findViewById(R.id.equal);
        btn_del = findViewById(R.id.deleteOne);

        textView_result = findViewById(R.id.displayText);
        //给按钮添加事件监听
        btn_zero.setOnClickListener(this);
        btn_one.setOnClickListener(this);
        btn_two.setOnClickListener(this);
        btn_three.setOnClickListener(this);
        btn_four.setOnClickListener(this);
        btn_five.setOnClickListener(this);
        btn_six.setOnClickListener(this);
        btn_seven.setOnClickListener(this);
        btn_eight.setOnClickListener(this);
        btn_night.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btn_sub.setOnClickListener(this);
        btn_mul.setOnClickListener(this);
        btn_div.setOnClickListener(this);
        btn_ac.setOnClickListener(this);
        btn_point.setOnClickListener(this);
        btn_eq.setOnClickListener(this);
        btn_del.setOnClickListener(this);
    }

    /**
     * 重写点击点击事件的方法
     * 将视图对象作为参数
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //按钮0
            case R.id.num0:
                if (string.indexOf("0") == 0 || string.indexOf(".") == -1) {

                } else {
                    string = string + "0";
                }
                textView_result.setText(string);
                break;
            //按钮1
            case R.id.num1:
                string = string + "1";
                textView_result.setText(string);
                break;
            //按钮2
            case R.id.num2:
                string = string + "2";
                textView_result.setText(string);
                break;
            //按钮3
            case R.id.num3:
                string = string + "3";
                textView_result.setText(string);
                break;
            //按钮4
            case R.id.num4:
                string = string + "4";
                textView_result.setText(string);
                break;
            //按钮5
            case R.id.num5:
                string = string + "5";
                textView_result.setText(string);
                break;
            //按钮6
            case R.id.num6:
                string = string + "6";
                textView_result.setText(string);
                break;
            //按钮7
            case R.id.num7:
                string = string + "7";
                textView_result.setText(string);
                break;
            //按钮8
            case R.id.num8:
                string = string + "8";
                textView_result.setText(string);
                break;
            //按钮9
            case R.id.num9:
                string = string + "9";
                textView_result.setText(string);
                break;
            //按钮.
            case R.id.dot:
                if (string.indexOf(".") == -1 && string.length() > 0) {
                    string = string + ".";
                    textView_result.setText(string);
                }
                break;
            case R.id.deleteOne:
                if(string.length() >= 1) {
                    string = string.substring(0, string.length()-1);
                    textView_result.setText(string);
                }
                break;
            //归0
            case R.id.clear:
                string = "";
                val_1 = "";
                val_2 = "";
                textView_result.setText("0");
                break;

            //除法
            case R.id.division:
                val_1 = string;
                symbol = "/";
                string = "";
                textView_result.setText(val_1);
                break;
            //乘法
            case R.id.multiply:
                val_1 = string;
                symbol = "*";
                string = "";
                textView_result.setText(val_1);
                break;
            //减法
            case R.id.subtraction:
                val_1 = string;
                symbol = "-";
                string = "";
                textView_result.setText(val_1);
                break;
            //加法
            case R.id.add:
                val_1 = string;
                symbol = "+";
                string = "";
                textView_result.setText(val_1);
                break;
            case R.id.equal:
                if ("/".equals(symbol)) {
                    string = (Double.valueOf(val_1) / Double.valueOf(string)) + "";
                    textView_result.setText(string);
                } else if ("*".equals(symbol)) {
                    string = (Double.valueOf(val_1) * Double.valueOf(string)) + "";
                    textView_result.setText(string);
                } else if ("-".equals(symbol)) {
                    string = (Double.valueOf(val_1) - Double.valueOf(string)) + "";
                    textView_result.setText(string);
                } else if ("+".equals(symbol)) {
                    string = (Double.valueOf(val_1) + Double.valueOf(string)) + "";
                    textView_result.setText(string);
                }
                break;
        }
    }
}