package com.tao.androidtest;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class DataBaseTest extends Activity {

    private DataBaseUtils dataBaseUtils;
    private Button add,update,select,delete;
    private EditText id,workId,workerName,workerAge;
    private SQLiteDatabase writeDatabase,readDatabase;
    private TextView result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.database);
        dataBaseUtils = new DataBaseUtils(this, "Worker", null, 1);
        this.init();
    }

    private void init() {
        add = findViewById(R.id.addData);
        update = findViewById(R.id.update);
        select = findViewById(R.id.select);
        id = findViewById(R.id.id);
        workId = findViewById(R.id.workId);
        workerName = findViewById(R.id.workerName);
        workerAge = findViewById(R.id.workerAge);
        delete = findViewById(R.id.delData);
        this.result = findViewById(R.id.DBResult);

        add.setOnClickListener(v -> {
            String inWorkId = workId.getText().toString();
            String inWorkerName = workerName.getText().toString();
            String inWorkerAge = workerAge.getText().toString();
            this.writeDatabase = dataBaseUtils.getWritableDatabase();
            this.writeDatabase.execSQL("insert into worker(workId, name, age) values ('" + inWorkId + "','"+inWorkerName+"','"+inWorkerAge+"')");
            writeDatabase.close();
            result.setText("添加成功!");
        });

        select.setOnClickListener(v -> {
            this.readDatabase = dataBaseUtils.getReadableDatabase();
            String inId = this.id.getText().toString();
            String sql = "select * from worker";
            Cursor cursor=null;
            if ("".equals(inId)) {
                this.result.setText("");
                StringBuilder temp = new StringBuilder();
                try {
                    cursor = this.readDatabase.query("worker", new String[] {"id", "workId", "name", "age"}, "", null, null, null, null);
                    while (cursor.moveToNext()){
                        temp.append(cursor.getInt(0)+",  "+cursor.getString(1)+", "+cursor.getString(2)+",  "+cursor.getString(3)+"\n");
                    }
                    this.result.setText(temp.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if (cursor!=null){
                        cursor.close();
                    }
                    this.readDatabase.close();
                }
            }else {
                try {
                    cursor = this.readDatabase.rawQuery("select * from worker where id = ?", new String[] {inId});
                    if (cursor.moveToNext()) {
                        result.setText("");
                        workId.setText(cursor.getString(1));
                        workerName.setText(cursor.getString(2));
                        workerAge.setText(cursor.getString(3));
                    }else {
                        result.setText("无此数据");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if (cursor!=null){
                        cursor.close();
                    }
                    this.readDatabase.close();
                }
            }
        });

        update.setOnClickListener(v -> {
            String inId = this.id.getText().toString();
            String inWorkId = workId.getText().toString();
            String inWorkerName = workerName.getText().toString();
            String inWorkerAge = workerAge.getText().toString();
            this.writeDatabase = dataBaseUtils.getWritableDatabase();
            if("".equals(inId) || "".equals(inWorkId) || "".equals(inWorkerName) || "".equals(inWorkerAge)) {
                this.result.setText("请先把数据填好哦");
                return;
            }
            String sql = "update worker set workId = ?, name = ?, age = ? where id = ?";
            this.writeDatabase.execSQL(sql, new String[] {inWorkId, inWorkerName, inWorkerAge, inId});
            this.writeDatabase.close();
            workId.setText("");
            workerName.setText("");
            workerAge.setText("");
            result.setText("更新成功！");
        });

        delete.setOnClickListener(v -> {
            String inId = this.id.getText().toString();
            this.writeDatabase = dataBaseUtils.getWritableDatabase();
            String sql = "delete from worker where id = ?";
            this.writeDatabase.execSQL(sql, new String[] {inId});
            this.writeDatabase.close();
            id.setText("");
            workId.setText("");
            workerName.setText("");
            workerAge.setText("");
            result.setText("删除成功！");
        });


    }
}
